from django.apps import AppConfig


class AdminEquipmentConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'admin_equipment'
