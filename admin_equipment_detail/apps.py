from django.apps import AppConfig


class AdminEquipmentDetailConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'admin_equipment_detail'
